function openForm(id) {
  document.getElementById(id).style.display = "flex";
  document.getElementById(id+"btnForm").style.display = "none";
  document.getElementById(id+"span").style.display = "none";
  document.getElementById(id+"new-date").style.display = "block";
}
function closeForm(id) {
  document.getElementById(id).style.display = "none";
  document.getElementById(id+"btnForm").style.display = "block";
  document.getElementById(id+"span").style.display = "block";
}
function newDate() {
  document.getElementById("new-date").style.display = "flex";
  document.getElementById("form-date").style.display = "none";
}
function closeDate() {
  document.getElementById("new-date").style.display = "none";
  document.getElementById("form-date").style.display = "block";
}
function choiceTr() {
  if(document.getElementById("monthly").style.display != "block") {
    document.getElementById("monthly").style.display = "block";
    document.getElementById("occasional").style.display = "none";
    var choice = 'Occasionnel';
    document.getElementById("choice").innerHTML=choice;
  } else {
    document.getElementById("monthly").style.display = "none";
    document.getElementById("occasional").style.display = "block";
    var choice = 'Mensuel';
    document.getElementById("choice").innerHTML=choice;
  }
}
function balanceSetting() {
  document.getElementById("balance").style.display = "none";
  document.getElementById("balBtn").style.display = "none";
  document.getElementById("balForm").style.display = "block";
}

let input = document.getElementById('trasaction-input')
let divChoice = document.getElementById('transactions-choice')
let choices = document.getElementsByClassName('transaction-choice')

// input.addEventListener('focus', () => {
//   console.log('TEST')
//   divChoice.style.display = null
// })
