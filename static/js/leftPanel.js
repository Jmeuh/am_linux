let open = document.getElementById('left-open')
let close = document.getElementById('left-close')
let panel = document.getElementById('left-panel')
let content = document.getElementById('left-content')

getWindowSize()

open.addEventListener('click', () => {
  // panel.classList.remove('left-panel')
  panel.classList.add('left-panel-open')
  open.style.display = 'none'
  content.style.width = '30%'

})

close.addEventListener('click', () => {
  panel.classList.remove('left-panel-open')
  panel.classList.add('left-panel-close')
  open.style.display = null
  content.style.width = '0%'
})

window.addEventListener('resize', () => {
  getWindowSize()
})

function getWindowSize() {
  if(window.innerWidth < 1000) {
    panel.classList.remove('left-panel')
    panel.classList.add('left-panel-close')
    open.style.display = null
    close.style.display = null
    content.style.width = '0%'

  } else {
    panel.classList.add('left-panel')
    panel.classList.remove('left-panel-close')
    open.style.display = 'none'
    close.style.display = 'none'
    content.style.width = '15%'
  }
}
