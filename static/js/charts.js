var negCats = document.getElementById("negCats").value
negCats = negCats.replaceAll("'", '"')

const datasList = JSON.parse(negCats)

const datasKey = []
const datas = []

for(const [k, v] of Object.entries(datasList)) {
  datasKey.push(k)
  datas.push(v)
}

colors = []
datas.forEach(data => {
  colors.push(getRandomColor())
})
avNegChart = document.getElementById('averageNegCat').getContext('2d')
var averageNegCat = new Chart( avNegChart, {
  type: 'doughnut',
  data: {
    labels: datasKey,
    datasets: [{
      data: datas,
      backgroundColor: colors,
      hoverOffset: 6,
    }]
  },
  options: {
    plugins: {
      legend: {
        onClick() {
          alert(Chart.data.datasets[0].label)
        },
        display: true,
        position: 'right',
        labels: {
          color: 'rgb(255, 255, 255)',
          font: {
            size: 16
          }
        }
      }
    }
  }
})

// var myChart = new Chart(
//   document.getElementById('myChart'),
//   {
//     type: 'pie',
//     data: {
//       labels: datasKey,
//       datasets: [{
//         data: datas,
//         backgroundColor: colors,
//         // hoverOffset:   4,
//       }]
//     },
//     options: {
//       plugins: {
//         legend: {
//           display: true,
//           position: 'right',
//           labels: {
//             color: 'rgb(255, 255, 255)',
//             font: {
//               size: 16
//             }
//           }
//         }
//       }
//     }
//   }
// )

// var div = document.getElementById("test")
// avNegChart.onclick = function(evt) { // C'est pas bon
//   console.log('TEST')
  // console.log(averageNegCat.getElementsAtEvent(evt))
  // avNegChart.style.display = "none"
  // div.style.display = null
  // => activePoints is an array of points on the canvas that are at the same position as the click event.
// }

function getRandomColor() {
  var letters = "0123456789ABCDEF".split("")
  var color = "#"
  for (var i = 0; i < 6; i++ ) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  return color
}

function change() {
  console.log("test fonction onclick")
  // var div = document.getElementById("test")
  // avNegChart.style.display = 'none'
  // div.style.display = null
}
